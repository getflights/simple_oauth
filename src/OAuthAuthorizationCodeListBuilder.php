<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of OAuth authorization code entities.
 *
 * @ingroup simple_oauth
 */
class OAuthAuthorizationCodeListBuilder extends EntityListBuilder
{

  /**
   * {@inheritdoc}
   */
  public function buildHeader()
  {
    $header['name'] = $this->t('Name');
    $header['owner'] = $this->t('Owner');
    $header['id'] = $this->t('ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity)
  {
    /** @var OAuthAuthorizationCodeInterface $entity */
    $row['name'] = Link::fromTextAndUrl($entity->label(), new Url('entity.oauth_authorization_code.edit_form', [
      'oauth_authorization_code' => $entity->id(),
    ]));

    $owner = $entity->{'owner_id'}->entity;
    $row['owner'] = Link::fromTextAndUrl($owner->label(), new Url('entity.user.canonical', [
      'user' => $owner->id(),
    ]));

    $row['id'] = $entity->id();

    return $row + parent::buildRow($entity);
  }
}
