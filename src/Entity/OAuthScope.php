<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\simple_oauth\OAuthScopeInterface;

/**
 * Defines the OAuthScope Entity
 *
 * @ConfigEntityType(
 *   id = "oauth_scope",
 *   label = @Translation("OAuth Scope"),
 *   handlers = {
 *     "list_builder" = "Drupal\simple_oauth\OAuthScopeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\simple_oauth\Entity\Form\OAuthScopeForm",
 *       "edit" = "Drupal\simple_oauth\Entity\Form\OAuthScopeForm",
 *       "delete" = "Drupal\simple_oauth\Entity\Form\OAuthScopeDeleteForm"
 *     },
 *     "access" = "Drupal\simple_oauth\LockableConfigEntityAccessControlHandler"
 *   },
 *   config_prefix = "oauth_scope",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/accounts/oauth_scope/{oauth_scope}",
 *     "edit-form" = "/admin/config/people/accounts/oauth_scope/{oauth_scope}/edit",
 *     "delete-form" = "/admin/config/people/accounts/oauth_scope/{oauth_scope}/delete",
 *     "collection" = "/admin/config/people/accounts/visibility_group"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "locked",
 *     "description",
 *   }
 * )
 */
class OAuthScope extends ConfigEntityBase implements OAuthScopeInterface
{
  protected string $id;
  protected string $label;
  protected string $description = '';
  protected bool $locked = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string
  {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description): self
  {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked(): bool
  {
    return $this->locked;
  }

  /**
   * {@inheritdoc}
   */
  public function lock(): self
  {
    $this->locked = TRUE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unlock(): self
  {
    $this->locked = FALSE;
    return $this;
  }
}
