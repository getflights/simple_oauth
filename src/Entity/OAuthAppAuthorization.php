<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\simple_oauth\OAuthAppAuthorizationInterface;
use Drupal\user\UserInterface;

/**
 * Defines the OAuth App Authorization entity. This entity is to keep track of which
 * users granted what app what scope permissions
 *
 * @ingroup simple_oauth
 *
 * @ContentEntityType(
 *   id = "oauth_app_authorization",
 *   label = @Translation("OAuth App Authorization"),
 *   handlers = {
 *     "view_builder" = "Drupal\simple_oauth\OAuthAppAuthorizationViewBuilder",
 *     "list_builder" = "Drupal\simple_oauth\OAuthAppAuthorizationListBuilder",
 *     "views_data" = "Drupal\simple_oauth\Entity\OAuthAppAuthorizationViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\simple_oauth\Entity\Form\OAuthAppAuthorizationForm",
 *       "add" = "Drupal\simple_oauth\Entity\Form\OAuthAppAuthorizationForm",
 *       "edit" = "Drupal\simple_oauth\Entity\Form\OAuthAppAuthorizationForm",
 *       "delete" = "Drupal\simple_oauth\Entity\Form\OAuthAppAuthorizationDeleteForm",
 *     },
 *     "access" = "Drupal\simple_oauth\OAuthAppAuthorizationAccessControlHandler",
 *   },
 *   base_table = "oauth_app_authorization",
 *   admin_permission = "administer OAuthAppAuthorization entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/oauth_app_authorization/{oauth_app_authorization}",
 *     "edit-form" = "/admin/config/people/oauth_app_authorization/{oauth_app_authorization}/edit",
 *     "delete-form" = "/admin/config/people/oauth_app_authorization/{oauth_app_authorization}/delete"
 *   }
 * )
 */
class OAuthAppAuthorization extends ContentEntityBase implements OAuthAppAuthorizationInterface
{
  use EntityChangedTrait;

  public function label()
  {
    return strtr(
      '@user - @app',
      [
        '@user' => $this->{'owner_id'}->entity->label(),
        '@app' => $this->{'oauth_app'}->entity->label()
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId()
  {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid)
  {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account)
  {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getScopeIds(): array
  {
    $scopeIds = [];

    foreach ($this->get('scopes') as $scopeValue) {
      $scopeIds[] = $scopeValue->target_id;
    }

    return $scopeIds;
  }

  /**
   * @param string[]|int[] $scopeIds
   * @return self
   */
  public function setScopeIds(array $scopeIds): self
  {
    $scopesAlreadyAdded = [];
    $index = 0;
    foreach ($this->{'scopes'} as $scopeValue) {
      if (!in_array($scopeValue->target_id, $scopeIds)) {
        $this->{'scopes'}->removeItem($index);
      } else {
        $scopeValue->target_id;
      }

      $index++;
    }

    foreach ($scopeIds as $scopeId) {
      if (!in_array($scopeId, $scopesAlreadyAdded)) {
        $this->{'scopes'}[] = ['target_id' => $scopeId];
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE)
  {
    parent::postSave($storage, $update);
  }

  public static function getCurrentUserId(): int
  {
    return \Drupal::currentUser()->id();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the App Authorization entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the App Authorization entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Creator'))
      ->setDescription(t('The user ID of the author of the App Authorization entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\OAuthAppAuthorization::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['owner_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The user ID of the user owning this application.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\OAuthAppAuthorization::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setPropertyConstraints('target_id', [
        'OwnOrAdmin' => [
          'permission' => 'administer oauth app entities',
        ],
      ]);

    $fields['oauth_app'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('App'))
      ->setDescription(t('The App that the authorization is for.'))
      ->setSetting('target_type', 'oauth_app')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'weight' => 4
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['scopes'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Scopes'))
      ->setDescription(t('The scopes for this App.'))
      ->setSetting('target_type', 'oauth_scope')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region'=> 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 4,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }
}
