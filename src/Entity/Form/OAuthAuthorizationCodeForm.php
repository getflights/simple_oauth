<?php

namespace Drupal\simple_oauth\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Access Token edit forms.
 *
 * @ingroup simple_oauth
 */
class OAuthAuthorizationCodeForm extends ContentEntityForm
{
  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $entity = $this->entity;
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label OAuth App.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label OAuth App.', [
          '%label' => $entity->label(),
        ]));
    }

    $form_state->setRedirect('user.oauth_authorization_code.collection', ['user' => $entity->{'owner_id'}->target_id]);
  }
}
