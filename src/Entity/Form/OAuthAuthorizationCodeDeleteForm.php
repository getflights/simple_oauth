<?php

namespace Drupal\simple_oauth\Entity\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\simple_oauth\OAuthAuthorizationCodeInterface;

/**
 * Provides a form for deleting Access Token entities.
 *
 * @ingroup simple_oauth
 */
class OAuthAuthorizationCodeDeleteForm extends ContentEntityConfirmFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getQuestion()
  {
    return $this->t('Are you sure you want to delete entity %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl()
  {
    /** @var OAuthAuthorizationCodeInterface $entity */
    $entity = $this->getEntity();
    return new Url('user.oauth_authorization_code.collection', [
      'user' => $entity->get('owner_id')->target_id,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText()
  {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->entity->delete();

    $this->messenger()->addMessage(
      $this->t(
        'content @type: deleted @label.',
        [
          '@type' => $this->entity->bundle(),
          '@label' => $this->entity->label()
        ]
      )
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
