<?php

namespace Drupal\simple_oauth\Entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_oauth\OAuthScopeInterface;

class OAuthScopeForm extends EntityForm
{
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {
    $form = parent::form($form, $form_state);

    /** @var OAuthScopeInterface $oauthScope */
    $oauthScope = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $oauthScope->label(),
      '#description' => $this->t('Label for the Scope.'),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $oauthScope->getDescription(),
      '#description' => $this->t('Description for the Scope.'),
      '#required' => FALSE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $oauthScope->id(),
      '#machine_name' => [
        'exists' => '\Drupal\simple_oauth\Entity\OAuthScope::load',
      ],
      '#disabled' => !$oauthScope->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    /** @var OAuthScopeInterface $oauthScope */
    $oauthScope = $this->getEntity();
    $status = $oauthScope->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Oauth Scope.', [
          '%label' => $oauthScope->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Oauth Scope.', [
          '%label' => $oauthScope->label(),
        ]));
    }

    $form_state->setRedirectUrl($oauthScope->toUrl('collection'));
  }
}
