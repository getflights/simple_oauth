<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\simple_oauth\AccessTokenResourceInterface;

/**
 * Defines the Access Token Resource entity.
 *
 * @ConfigEntityType(
 *   id = "access_token_resource",
 *   label = @Translation("Access Token Resource"),
 *   handlers = {
 *     "list_builder" = "Drupal\simple_oauth\AccessTokenResourceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\simple_oauth\Entity\Form\AccessTokenResourceForm",
 *       "edit" = "Drupal\simple_oauth\Entity\Form\AccessTokenResourceForm",
 *       "delete" = "Drupal\simple_oauth\Entity\Form\AccessTokenResourceDeleteForm"
 *     },
 *     "access" = "Drupal\simple_oauth\LockableConfigEntityAccessControlHandler"
 *   },
 *   config_prefix = "access_token_resource",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/accounts/access_token_resource/{access_token_resource}",
 *     "edit-form" = "/admin/config/people/accounts/access_token_resource/{access_token_resource}/edit",
 *     "delete-form" = "/admin/config/people/accounts/access_token_resource/{access_token_resource}/delete",
 *     "collection" = "/admin/config/people/accounts/visibility_group"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "locked",
 *     "description",
 *   }
 * )
 */
class AccessTokenResource extends ConfigEntityBase implements AccessTokenResourceInterface
{
  protected string $id;
  protected string $label;
  protected string $description = '';
  protected bool $locked = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string
  {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description): self
  {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked(): bool
  {
    return $this->locked;
  }

  /**
   * {@inheritdoc}
   */
  public function lock(): self
  {
    $this->locked = TRUE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unlock(): self
  {
    $this->locked = FALSE;
    return $this;
  }
}
