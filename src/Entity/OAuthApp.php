<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\link\LinkItemInterface;
use Drupal\simple_oauth\OAuthAppInterface;
use Drupal\simple_oauth\Services\OAuthServiceInterface;
use Drupal\user\UserInterface;
use ReflectionClass;

/**
 * Defines the OAuth App entity.
 *
 * @ingroup simple_oauth
 *
 * @ContentEntityType(
 *   id = "oauth_app",
 *   label = @Translation("OAuth App"),
 *   handlers = {
 *     "view_builder" = "Drupal\simple_oauth\OAuthAppViewBuilder",
 *     "list_builder" = "Drupal\simple_oauth\OAuthAppListBuilder",
 *     "views_data" = "Drupal\simple_oauth\Entity\OAuthAppViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\simple_oauth\Entity\Form\OAuthAppForm",
 *       "add" = "Drupal\simple_oauth\Entity\Form\OAuthAppForm",
 *       "edit" = "Drupal\simple_oauth\Entity\Form\OAuthAppForm",
 *       "delete" = "Drupal\simple_oauth\Entity\Form\OAuthAppDeleteForm",
 *     },
 *     "access" = "Drupal\simple_oauth\OAuthAppAccessControlHandler",
 *   },
 *   base_table = "oauth_app",
 *   admin_permission = "administer OAuthApp entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/oauth_app/{oauth_app}",
 *     "edit-form" = "/admin/config/people/oauth_app/{oauth_app}/edit",
 *     "delete-form" = "/admin/config/people/oauth_app/{oauth_app}/delete"
 *   }
 * )
 */
class OAuthApp extends ContentEntityBase implements OAuthAppInterface
{
  use EntityChangedTrait;

  const REFRESH_EXPIRATION_DAILY = 'daily';
  const REFRESH_EXPIRATION_48H = '48h';
  const REFRESH_EXPIRATION_WEEKLY = 'weekly';
  const REFRESH_EXPIRATION_BIWEEKLY = 'biweekly';
  const REFRESH_EXPIRATION_MONTHLY = 'monthly';
  const REFRESH_EXPIRATION_BIMONTHLY = 'bimonthly';
  const REFRESH_EXPIRATION_SEMIANNUAL = 'semiannual';
  const REFRESH_EXPIRATION_ANNUAL = 'annual';
  const STATUS_ACTIVE = 'active';
  const STATUS_DISABLED = 'disabled';

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
  {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => self::getCurrentUserId(),
      'client_id' => 'public_' . bin2hex(random_bytes(32)),
      'client_secret' => 'secret_' . bin2hex(random_bytes(72))
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getExpirationInSeconds(): int
  {
    // 60*60*24
    $daily = 86400;
    $refreshExpiration = $this->getRefreshExpiration();

    if ($refreshExpiration === self::REFRESH_EXPIRATION_48H) {
      return ($daily * 2);
    } else if ($refreshExpiration === self::REFRESH_EXPIRATION_WEEKLY) {
      return ($daily * 7);
    } else if ($refreshExpiration === self::REFRESH_EXPIRATION_BIWEEKLY) {
      return ($daily * 14);
    } else if ($refreshExpiration === self::REFRESH_EXPIRATION_MONTHLY) {
      return ($daily * 30);
    } else if ($refreshExpiration === self::REFRESH_EXPIRATION_BIMONTHLY) {
      return ($daily * 61);
    } else if ($refreshExpiration === self::REFRESH_EXPIRATION_SEMIANNUAL) {
      return ($daily * 183);
    } else if ($refreshExpiration === self::REFRESH_EXPIRATION_ANNUAL) {
      return ($daily * 365);
    }

    return $daily;
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshExpiration(): string
  {
    return $this->get('refresh_expiration')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCallbackUrl(): string
  {
    return $this->get('callback_url')->uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getScopeIds(): array
  {
    $scopeIds = [];

    foreach ($this->get('scopes') as $scopeValue) {
      $scopeIds[] = $scopeValue->target_id;
    }

    return $scopeIds;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId()
  {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid)
  {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive(): bool
  {
    return $this->get('status')->value === self::STATUS_ACTIVE;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account)
  {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE)
  {
    parent::postSave($storage, $update);
    $this->deleteAccessTokensTrigger();
  }

  public static function getCurrentUserId(): int
  {
    return \Drupal::currentUser()->id();
  }

  /**
   * Delete access tokens when inactive
   *
   * @return self
   */
  public function deleteAccessTokensTrigger(): self
  {
    if ($this->origial) {
      /** @var OAuthAppInterface $originalEntity */
      $originalEntity = $this->original;

      if ($originalEntity->isActive() && $this->isActive()) {
        /** @var OAuthServiceInterface $service */
        $service = \Drupal::service('simple_oauth.oauth_service');
        $service->removeAccessTokensForApp($this);
      }
    } else if (!$this->isActive()) {
      /** @var OAuthServiceInterface $service */
      $service = \Drupal::service('simple_oauth.oauth_service');
      $service->removeAccessTokensForApp($this);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the App entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the App entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Creator'))
      ->setDescription(t('The user ID of the author of the App entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\OAuthApp::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The Name of the App entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'text_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 6,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['owner_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The user ID of the user owning this application.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\OAuthApp::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setPropertyConstraints('target_id', [
        'OwnOrAdmin' => [
          'permission' => 'administer oauth app entities',
        ],
      ]);

    $fields['callback_url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Callback URL'))
      ->setDescription(t('The location to where the user will be redirected upon login.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_EXTERNAL,
        'title' => DRUPAL_DISABLED,
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => 0,
      ]);

    $fields['scopes'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Scopes'))
      ->setDescription(t('The scopes for this App.'))
      ->setSetting('target_type', 'oauth_scope')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 4,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['client_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Client ID'))
      ->setDescription(t('The Client ID that can be publicly known to consumers.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['client_secret'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Client Secret'))
      ->setDescription(t('The Client Secret that must only be passed by authorized applications.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['refresh_expiration'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Refresh Token Expiration'))
      ->setDescription(t('How long the refresh token is active'))
      ->setSetting('allowed_values', self::getAllowedValues('REFRESH_EXPIRATION_'))
      ->setDefaultValue(self::REFRESH_EXPIRATION_DAILY)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the App'))
      ->setSetting('allowed_values', self::getAllowedValues('STATUS_'))
      ->setDefaultValue(self::STATUS_ACTIVE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    return $fields;
  }

  /**
   * @return array
   * @throws ReflectionException
   */
  public static function getAllowedValues(string $prefix): array
  {
    $allowedValues = [];

    $class = new ReflectionClass(self::class);
    if ($constants = $class->getConstants()) {
      foreach ($constants as $name => $value) {
        if (strpos($name, $prefix) === 0) {
          $allowedValues[$value] = $value;
        }
      }
    }

    return array_unique($allowedValues);
  }
}
