<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for OAuth App entities.
 */
class OAuthAppAuthorizationViewsData extends EntityViewsData implements EntityViewsDataInterface
{
  /**
   * {@inheritdoc}
   */
  public function getViewsData()
  {
    $data = parent::getViewsData();

    $data['oauth_app_authorization']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('OAuth App Authorization'),
      'help' => $this->t('The OAuth App Authorization ID.'),
    ];

    return $data;
  }
}
