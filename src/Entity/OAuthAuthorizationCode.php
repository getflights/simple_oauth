<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\simple_oauth\OAuthAuthorizationCodeInterface;
use Drupal\simple_oauth\Services\OAuthServiceInterface;
use Drupal\user\UserInterface;

/**
 * Defines the OAuth Authorization Code entity.
 *
 * @ingroup simple_oauth
 *
 * @ContentEntityType(
 *   id = "oauth_authorization_code",
 *   label = @Translation("OAuth Authorization Code"),
 *   handlers = {
 *     "view_builder" = "Drupal\simple_oauth\OAuthAuthorizationCodeViewBuilder",
 *     "list_builder" = "Drupal\simple_oauth\OAuthAuthorizationCodeListBuilder",
 *     "views_data" = "Drupal\simple_oauth\Entity\OAuthAuthorizationCodeViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\simple_oauth\Entity\Form\OAuthAuthorizationCodeForm",
 *       "add" = "Drupal\simple_oauth\Entity\Form\OAuthAuthorizationCodeForm",
 *       "edit" = "Drupal\simple_oauth\Entity\Form\OAuthAuthorizationCodeForm",
 *       "delete" = "Drupal\simple_oauth\Entity\Form\OAuthAuthorizationCodeDeleteForm",
 *     },
 *     "access" = "Drupal\simple_oauth\OAuthAuthorizationCodeAccessControlHandler",
 *   },
 *   base_table = "oauth_authorization_code",
 *   admin_permission = "administer oauth authorization code entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "value",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/oauth_authorization_code/{oauth_authorization_code}",
 *     "edit-form" = "/admin/config/people/oauth_authorization_code/{oauth_authorization_code}/edit",
 *     "delete-form" = "/admin/config/people/oauth_authorization_code/{oauth_authorization_code}/delete"
 *   }
 * )
 */
class OAuthAuthorizationCode extends ContentEntityBase implements OAuthAuthorizationCodeInterface
{
  use EntityChangedTrait;

  public function preSave(EntityStorageInterface $storage)
  {
    // Create the token value as a digestion of the values in the token. This
    // will allow us to check integrity of the token later.
    $this->set('value', bin2hex(random_bytes(24)));
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpire(): int
  {
    return $this->get('expire')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(): string
  {
    return $this->get('value')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeChallenge(): ?string
  {
    return $this->get('code_challenge')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeChallengeMethod(): ?string
  {
    return $this->get('code_challenge_method')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId()
  {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid)
  {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account)
  {
    $this->set('user_id', $account->id());
    return $this;
  }

  public static function getCurrentUserId(): int
  {
    return \Drupal::currentUser()->id();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultExpiration()
  {
    /** @var OAuthServiceInterface $service */
    $service = \Drupal::service("simple_oauth.oauth_service");
    $expiration = $service->getAuthorizationCodeExpirationTime();
    return [\Drupal::time()->getRequestTime() + $expiration];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Authorization Code entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Authorization Code entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Creator'))
      ->setDescription(t('The user ID of the author of the Authorization Code entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\OAuthAuthorizationCode::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['oauth_app'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('App'))
      ->setDescription(t('The App that requested the Authorization Code.'))
      ->setSetting('target_type', 'oauth_app')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'weight' => 4
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['owner_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The user ID of the user owning this authorization code.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\OAuthAuthorizationCode::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setPropertyConstraints('target_id', [
        'OwnOrAdmin' => [
          'permission' => 'administer oauth authorization codes entities',
        ],
      ]);

    $fields['expire'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expire'))
      ->setDefaultValueCallback(__CLASS__ . '::defaultExpiration')
      ->setDescription(t('The time when the token expires.'))
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 1,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['value'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value'))
      ->setDescription(t('The authorization code.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['code_challenge'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code Challenge'))
      ->setDescription(t('The code challenge used for the authorization of this app.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['code_challenge_method'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code Challenge Method'))
      ->setDescription(t('The method used for hashing the code challenge.'))
      ->setSettings([
        'max_length' => 25,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    return $fields;
  }
}
