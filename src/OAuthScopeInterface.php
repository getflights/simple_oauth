<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining OAuth Scope entities.
 */
interface OAuthScopeInterface extends LockableEntityInterface
{
  /**
   * @return string
   */
  public function getDescription(): string;

  /**
   * @param string $description
   * @return self
   */
  public function setDescription(string $description): self;
}
