<?php

namespace Drupal\simple_oauth\Services;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\simple_oauth\AccessTokenInterface;
use Drupal\simple_oauth\Authentication\TokenAuthUser;
use Drupal\simple_oauth\Authentication\TokenAuthUserInterface;
use Drupal\simple_oauth\Entity\OAuthApp;
use Drupal\simple_oauth\Entity\OAuthAuthorizationCode;
use Drupal\simple_oauth\Exceptions\InvalidAuthorizationCodeException;
use Drupal\simple_oauth\Exceptions\InvalidRefreshTokenException;
use Drupal\simple_oauth\Exceptions\RefreshTokenAlreadyExistsException;
use Drupal\simple_oauth\Exceptions\ScopeMissingException;
use Drupal\simple_oauth\OAuthAppAuthorizationInterface;
use Drupal\simple_oauth\OAuthAppInterface;
use Drupal\simple_oauth\OAuthAuthorizationCodeInterface;
use Drupal\simple_oauth\OAuthScopeInterface;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class OAuthService implements OAuthServiceInterface
{
  private ImmutableConfig $oauthConfig;

  /**
   * The default time while the token is valid.
   *
   * @var int
   */
  const DEFAULT_EXPIRATION_PERIOD = 300;

  /**
   * The time a refresh token stays valid after the access token expires.
   *
   * @var int
   */
  const REFRESH_EXTENSION_TIME = 86400;


  public function __construct(
    private readonly LoggerInterface $logger,
    private readonly TimeInterface $time,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
    private readonly RequestStack $requestStack,
    private readonly AccountInterface $currentUser
  ) {
    $this->oauthConfig = $configFactory->get('simple_oauth.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultAccessTokenExpirationTime(): int
  {
    return $this->oauthConfig->get('expiration') ?: static::DEFAULT_EXPIRATION_PERIOD;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRefreshTokenExpirationTime(): int
  {
    return $this->oauthConfig->get('refresh_extension') ?: static::REFRESH_EXTENSION_TIME;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationCodeExpirationTime(): int
  {
    return $this->oauthConfig->get('authorization_code_expiration') ?: static::REFRESH_EXTENSION_TIME;
  }

  /**
   * {@inheritdoc}
   */
  public function removeAllTokensForUser(UserInterface $user): self
  {
    $store = $this->entityTypeManager->getStorage('access_token');
    $tokenIds = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('auth_user_id', $user->id() ?? 'NOID')
      ->execute();

    if (!empty($tokenIds)) {
      $tokens = $store->loadMultiple($tokenIds);
      foreach ($tokens as $token) {
        $token->delete();
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeAccessTokensForApp(OAuthAppInterface $app): self
  {
    $store = $this->entityTypeManager->getStorage('access_token');
    $tokenIds = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('oauth_app.target_id', $app->id() ?? 'NOID')
      ->execute();

    if (!empty($tokenIds)) {
      $tokens = $store->loadMultiple($tokenIds);
      foreach ($tokens as $token) {
        $token->delete();
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeAccessTokensForSpecificUserAndApp(OAuthAppInterface $app, UserInterface $user): self
  {
    $store = $this->entityTypeManager->getStorage('access_token');
    $tokenIds = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('auth_user_id', $user->id() ?? 'NOID')
      ->condition('oauth_app.target_id', $app->id() ?? 'NOID')
      ->execute();

    if (!empty($tokenIds)) {
      $tokens = $store->loadMultiple($tokenIds);
      foreach ($tokens as $token) {
        $token->delete();
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshRefreshToken(AccessTokenInterface $refreshToken): AccessTokenInterface
  {
    if (!$refreshToken->isRefreshToken()) {
      $this->logger->error(strtr(
        'User @userId tried refreshing a token that was an access token instead of a refresh token',
        [
          '@userId' => $refreshToken->get('auth_user_id')
        ]
      ));
      throw new InvalidRefreshTokenException();
    }

    // Find / generate the access token for this refresh token.
    $accessToken = $refreshToken->get('access_token_id')->entity;

    $scopeValue = [];
    foreach ($refreshToken->get('scopes') as $scope) {
      $scopeValue[] = ['target_id' => $scope->{'target_id'}];
    }

    /** @var OAuthApp|null $oauthApp */
    $oauthApp = $refreshToken->get('oauth_app')->entity;

    $values = [
      'expire' => $this->time->getRequestTime() + $this->getDefaultAccessTokenExpirationTime(),
      'user_id' => $refreshToken->get('user_id')->target_id,
      'auth_user_id' => $refreshToken->get('auth_user_id')->target_id,
      'scopes' => $scopeValue,
      'resource' => $accessToken ? $accessToken->get('resource')->target_id : 'global',
      'created' => $this->time->getRequestTime(),
      'changed' => $this->time->getRequestTime(),
    ];

    if (!empty($oauthApp)) {
      if (!$oauthApp->isActive()) {
        $this->logger->error(strtr(
          'User @userId tried refreshing a token that was linked to an app (@appId) that is no longer active',
          [
            '@userId' => $refreshToken->get('auth_user_id'),
            '@appId' => $oauthApp->id()
          ]
        ));
        throw new InvalidRefreshTokenException();
      }

      $values['oauth_app'] = $oauthApp->id();
    }

    /** @var AccessTokenInterface $accessToken */
    $store = $this->entityTypeManager->getStorage('access_token');
    $accessToken = $store->create($values);
    $this->setRequestValuesOnAccessToken($accessToken);

    // Lets also fetch any access tokens linked to the refresh token
    // We must delete those old values
    $oldTokenIds = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', $refreshToken->get('access_token_id')->target_id ?? 'NOID')
      ->execute();

    if (!empty($oldTokenIds)) {
      $oldTokens = $store->loadMultiple($oldTokenIds);
      foreach ($oldTokens as $oldToken) {
        $oldToken->delete();
      }
    }

    // This refresh token is no longer needed.
    $refreshToken->delete();

    // Saving this token will generate a new linked refresh token in the trigger of access token.
    $accessToken->save();

    return $accessToken;
  }

  /**
   * {@inheritdoc}
   */
  public function removeExpiredTokens(): self
  {
    $storage = $this->entityTypeManager->getStorage('access_token');
    $query = $storage->getQuery()->accessCheck(FALSE);
    // We only delete access tokens when deleting their expired refresh tokens.
    $ids = $query
      ->condition('expire', $this->time->getRequestTime(), '<')
      ->condition('resource', 'authentication')
      ->execute();

    if (!empty($ids)) {
      $refreshTokens = $storage->loadMultiple($ids);
      // Get the access tokens associated to this refresh token.
      $accessTokens = array_map(function ($refreshToken) {
        /** @var AccessTokenInterface $refreshToken */
        return $refreshToken->get('access_token_id')->entity;
      }, $refreshTokens);
      // Delete the access tokens.
      $storage->delete(array_filter($accessTokens));
      // Delete the refresh tokens.
      $storage->delete($refreshTokens);
    }

    return $this;
  }

  /**
   * @return self
   */
  public function removeExpiredAuthorizationCodes(): self
  {
    $storage = $this->entityTypeManager->getStorage('oauth_authorization_code');
    $query = $storage->getQuery()->accessCheck(FALSE);
    // We only delete access tokens when deleting their expired refresh tokens.
    $ids = $query
      ->condition('expire', $this->time->getRequestTime(), '<')
      ->execute();

    if (!empty($ids)) {
      $authorizationCodes = $storage->loadMultiple($ids);
      // Delete the refresh tokens.
      $storage->delete($authorizationCodes);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function generateAccessTokenForUser(UserInterface $user, array $scopes): AccessTokenInterface
  {
    $store = $this->entityTypeManager->getStorage('access_token');

    if (sizeof($scopes) === 0) {
      throw new ScopeMissingException('No scopes given for access token');
    }

    foreach ($scopes as $scope) {
      if (!$scope instanceof OAuthScopeInterface) {
        throw new ScopeMissingException('Passed scope does not implement OAuthScopeInterface');
      }
    }

    $scopeValue = [];
    foreach ($scopes as $scope) {
      $scopeValue[] = ['target_id' => $scope->id()];
    }

    $values = [
      'expire' => $this->time->getRequestTime() + $this->getDefaultAccessTokenExpirationTime(),
      'user_id' => $user->id(),
      'auth_user_id' => $user->id(),
      'scopes' => $scopeValue,
      'resource' => 'global',
      'created' => $this->time->getRequestTime(),
      'changed' => $this->time->getRequestTime(),
    ];

    /** @var AccessTokenInterface $token */
    $token = $store->create($values);
    $this->setRequestValuesOnAccessToken($token);
    $token->save();

    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestValuesOnAccessToken(AccessTokenInterface $accessToken): self
  {
    $request = $this->requestStack->getCurrentRequest();

    $accessToken
      ->setIp($request->getClientIp())
      ->setUserAgent($request->headers->get('User-Agent'));

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function generateRefreshTokenForAccessToken(AccessTokenInterface $accessToken): AccessTokenInterface
  {
    $store = $this->entityTypeManager->getStorage('access_token');

    // Only add the refresh token if there is none associated.
    $refreshTokenExists = (bool) $store->getQuery()
      ->accessCheck(FALSE)
      ->condition('access_token_id', $accessToken->id())
      ->count()
      ->execute();

    if ($refreshTokenExists) {
      throw new RefreshTokenAlreadyExistsException();
    }

    $scopeValue = [];
    if ($scopes = $accessToken->get('scopes')) {
      foreach ($scopes as $scope) {
        $scopeValue[] = ['target_id' => $scope->{'target_id'}];
      }

      if (sizeof($scopeValue) === 0) {
        $scopeValue = null;
      }
    }

    /** @var OAuthApp|null $oauthApp */
    $oauthApp = $accessToken->get('oauth_app')->entity;

    $values = [
      'expire' => $accessToken->get('expire')->value + $this->getDefaultRefreshTokenExpirationTime(),
      'user_id' => $accessToken->get('user_id')->target_id,
      'auth_user_id' => $accessToken->get('auth_user_id')->target_id,
      'scopes' => $scopeValue,
      'access_token_id' => $accessToken->id(),
      'resource' => 'authentication',
      'created' => $accessToken->getCreatedTime(),
      'changed' => $accessToken->getChangedTime(),
    ];

    if (!empty($oauthApp)) {
      $values['oauth_app'] = $oauthApp->id();
      $values['expire'] = $accessToken->get('expire')->value + $oauthApp->getExpirationInSeconds();
    }

    $refreshToken = $store->create($values);
    $this->setRequestValuesOnAccessToken($refreshToken);
    $refreshToken->save();

    return $refreshToken;
  }

  /**
   * {@inheritdoc}
   */
  public function generateAuthorizationCode(AccountInterface $user, OAuthAppInterface $oauthApp): OAuthAuthorizationCodeInterface
  {
    $store = $this->entityTypeManager->getStorage('oauth_authorization_code');

    $expiration = $this->getAuthorizationCodeExpirationTime();

    $scopeValue = [];
    if ($scopes = $oauthApp->get('scopes')) {
      foreach ($scopes as $scope) {
        $scopeValue[] = ['target_id' => $scope->{'target_id'}];
      }

      if (sizeof($scopeValue) === 0) {
        throw new ScopeMissingException();
      }
    }

    $values = [
      'expire' => $this->time->getRequestTime() + $expiration,
      'user_id' => $user->id(),
      'owner_id' => $user->id(),
      'oauth_app' => $oauthApp->id(),
      'scopes' => $scopeValue,
      'created' => $this->time->getRequestTime(),
      'changed' => $this->time->getRequestTime(),
    ];

    $authorizationCode = $store->create($values);
    $authorizationCode->save();

    return $authorizationCode;
  }

  /**
   * {@inheritdoc}
   */
  public function getScopeById(string $id): ?OAuthScopeInterface
  {
    $store = $this->entityTypeManager->getStorage('oauth_scope');
    return $store->load($id);
  }

  /**
   * @param string $token
   * @return self
   */
  public function revokeToken(string $token): self
  {
    $store = $this->entityTypeManager->getStorage('access_token');
    $tokenIds = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('value', $token)
      ->execute();

    if (!empty($tokenIds)) {
      $tokens = $store
        ->loadMultiple($tokenIds);
      foreach ($tokens as $token) {
        $token->delete();
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserByTokenValue(string $token): ?TokenAuthUserInterface
  {
    $store = $this->entityTypeManager->getStorage('access_token');

    $ids = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('value', $token)
      ->condition('expire', $this->time->getRequestTime(), '>')
      ->range(0, 1)
      ->execute();

    if (!empty($ids)) {
      /** @var AccessTokenInterface $token */
      $token = $store->load(reset($ids));
      return new TokenAuthUser($token);
    }

    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenFromRequest(Request $request): ?string
  {
    // Check the header. See: http://tools.ietf.org/html/rfc6750#section-2.1
    $auth_header = $request->headers->get('Authorization', '', TRUE);
    $prefix = 'Bearer ';

    if (strpos($auth_header, $prefix) === 0) {
      return substr($auth_header, strlen($prefix));
    }

    // Form encoded parameter. See:
    // http://tools.ietf.org/html/rfc6750#section-2.2
    $ct_header = $request->headers->get('Content-Type', '', TRUE);
    $is_get = $request->getMethod() == Request::METHOD_GET;

    $token = $request->request->get('access_token');
    if (!$is_get && $ct_header == 'application/x-www-form-urlencoded' && $token) {
      return $token;
    }

    // This module purposely refuses to implement
    // http://tools.ietf.org/html/rfc6750#section-2.3 for security resons.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAppByClientId(string $clientId): ?OAuthAppInterface
  {
    $store = $this->entityTypeManager->getStorage('oauth_app');

    $ids = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('client_id', $clientId)
      ->condition('status', OAuthApp::STATUS_ACTIVE)
      ->range(0, 1)
      ->execute();

    if (!empty($ids)) {
      return $store->load(reset($ids));
    }

    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizedScopesForAppByUser(string $userId, string $appId): array
  {
    $scopeIds = [];

    $store = $this->entityTypeManager->getStorage('oauth_app_authorization');
    $ids = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('oauth_app', $appId)
      ->condition('owner_id', $userId)
      ->range(0, 1)
      ->execute();

    if (!empty($ids)) {
      /** @var OAuthAppAuthorizationInterface $appAuthorization */
      $appAuthorization = $store->load(reset($ids));

      foreach ($appAuthorization->getScopeIds() as $scopeId) {
        $scopeIds[] = $scopeId;
      }
    }

    return $scopeIds;
  }

  /**
   * {@inheritdoc}
   */
  public function didUserAuthorizeApp(AccountInterface $user, OAuthAppInterface $app): bool
  {
    $appScopes = $app->getScopeIds();
    $authorizedScopes = $this->getAuthorizedScopesForAppByUser($user->id(), $app->id());

    return !array_diff($appScopes, $authorizedScopes);
  }

  /**
   * Deletes all the app authorizations for a user/app combo
   *
   * @param OAuthAppInterface $app
   * @param AccountInterface $user
   * @return self
   */
  private function removeAppAuthorizationsForUser(OAuthAppInterface $app, AccountInterface $user): self
  {
    $store = $this->entityTypeManager->getStorage('oauth_app_authorization');
    $ids = $store
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('oauth_app', $app->id())
      ->condition('owner_id', $user->id())
      ->execute();

    if (!empty($ids)) {
      /** @var OAuthAppAuthorizationInterface $appAuthorization */
      $appAuthorizations = $store->loadMultiple($ids);

      foreach ($appAuthorizations as $appAuthorization) {
        $appAuthorization->delete();
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function deauthorizeAppForUser(OAuthAppInterface $app, AccountInterface $user): self
  {
    $this->removeAppAuthorizationsForUser($app, $user);
    $this->removeAccessTokensForSpecificUserAndApp($app, $user);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function authorizeAppForUser(OAuthAppInterface $app, AccountInterface $user): OAuthAppAuthorizationInterface
  {
    $this->removeAppAuthorizationsForUser($app, $user);

    $store = $this->entityTypeManager->getStorage('oauth_app_authorization');

    $values = [
      'user_id' => $user->id(),
      'owner_id' => $user->id(),
      'oauth_app' => $app->id(),
      'scopes' => $app->getScopeIds()
    ];

    $appAuthorization = $store->create($values);
    $appAuthorization->save();

    return $appAuthorization;
  }

  /**
   * {@inheritdoc}
   */
  public function createAuthorizationCodeForUser(
    AccountInterface $user,
    OAuthAppInterface $app,
    ?string $codeChallenge = null,
    ?string $codeChallengeMethod = null
  ): OAuthAuthorizationCodeInterface {
    $values = [
      'expire' => $this->time->getRequestTime() + $this->getAuthorizationCodeExpirationTime(),
      'user_id' => $this->currentUser->id(),
      'owner_id' => $user->id(),
      'oauth_app' => $app->id(),
      'code_challenge' => $codeChallenge,
      'code_challenge_method' => $codeChallengeMethod,
      'created' => $this->time->getRequestTime(),
      'changed' => $this->time->getRequestTime(),
    ];

    /** @var OAuthAuthorizationCodeInterface $authCode */
    $store = $this->entityTypeManager->getStorage('oauth_authorization_code');
    $authCode = $store->create($values);

    return $authCode;
  }

  /**
   * {@inheritdoc}
   */
  public function generateAccessTokenByAuthorizationCode(OAuthAuthorizationCode $authorizationCode): AccessTokenInterface
  {
    $store = $this->entityTypeManager->getStorage('access_token');

    if ($authorizationCode->getExpire() < $this->time->getRequestTime()) {
      throw new InvalidAuthorizationCodeException('Authorization Code expired');
    }

    /** @var OAuthApp $app */
    $app = $authorizationCode->get('oauth_app')->entity;
    if (empty($app)) {
      throw new InvalidAuthorizationCodeException('Authorization Code not linked to app');
    }

    $scopeValue = [];
    foreach ($app->getScopeIds() as $scopeId) {
      $scopeValue[] = ['target_id' => $scopeId];
    }

    $values = [
      'expire' => $this->time->getRequestTime() + $this->getDefaultAccessTokenExpirationTime(),
      'user_id' => $authorizationCode->getOwnerId(),
      'auth_user_id' => $authorizationCode->getOwnerId(),
      'oauth_app' => $app->id(),
      'scopes' => $scopeValue,
      'resource' => 'global',
      'created' => $this->time->getRequestTime(),
      'changed' => $this->time->getRequestTime(),
    ];

    /** @var AccessTokenInterface $token */
    $token = $store->create($values);
    $this->setRequestValuesOnAccessToken($token);
    $token->save();

    // And finally delete the authorization code, it can only be used once to generate an access token
    $authorizationCode->delete();

    return $token;
  }
}
