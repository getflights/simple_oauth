<?php

namespace Drupal\simple_oauth;

/**
 * Provides an interface for defining Access Token Resource entities.
 */
interface AccessTokenResourceInterface extends LockableEntityInterface
{
  /**
   * @return string
   */
  public function getDescription(): string;

  /**
   * @param string $description
   * @return self
   */
  public function setDescription(string $description): self;
}
