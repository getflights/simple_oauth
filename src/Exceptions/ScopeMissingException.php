<?php

namespace Drupal\simple_oauth\Exceptions;

class ScopeMissingException extends \Exception
{
}
