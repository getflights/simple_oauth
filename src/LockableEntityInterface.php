<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Lockable Entity entities.
 */
interface LockableEntityInterface extends ConfigEntityInterface
{
  /**
   * @return boolean
   */
  public function isLocked(): bool;

  /**
   * @return self
   */
  public function lock(): self;

  /**
   * @return self
   */
  public function unlock(): self;
}
