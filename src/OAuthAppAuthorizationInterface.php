<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining OAuth Apps.
 *
 * @ingroup simple_oauth
 */
interface OAuthAppAuthorizationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface
{
  /**
   * @return integer
   */
  public function getCreatedTime(): int;

  /**
   * Returns all the scope ids for the app
   *
   * @return string[]
   */
  public function getScopeIds(): array;
}
