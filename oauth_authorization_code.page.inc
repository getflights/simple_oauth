<?php

/**
 * @file
 * Contains oauth_authorization_code.page.inc..
 *
 * Page callback for OAuth App entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for OAuth App templates.
 *
 * Default template: oauth-authorization-code.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oauth_authorization_code(array &$variables)
{
  // Fetch OAuth App Entity Object.
  $oauthAuthorizationCode = $variables['oauth_authorization_code'];

  $variables['attributes']['class'] = empty($variables['attributes']['class']) ? [] : $variables['attributes']['class'];
  $variables['attributes']['class'][] = 'oauth-authorization-code';

  // Helpful $content variable for templates.
  foreach (Element::children($oauthAuthorizationCode) as $key) {
    $variables['content'][$key] = $oauthAuthorizationCode[$key];
  }
}
