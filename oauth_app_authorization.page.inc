<?php

/**
 * @file
 * Contains oauth_app_authorization.page.inc..
 *
 * Page callback for OAuth App Authorization entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for OAuth App Authorization templates.
 *
 * Default template: oauth-app-authorization.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oauth_app_authorization(array &$variables) {
  // Fetch OAuth App Authorization Entity Object.
  $oauthAppAuthorization = $variables['oauth_app_authorization'];

  $variables['attributes']['class'] = empty($variables['attributes']['class']) ? [] : $variables['attributes']['class'];
  $variables['attributes']['class'][] = 'oauth-app-authorization';

  // Helpful $content variable for templates.
  foreach (Element::children($oauthAppAuthorization) as $key) {
    $variables['content'][$key] = $oauthAppAuthorization[$key];
  }
}
