<?php

use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\simple_oauth\Entity\AccessToken;
use Drupal\simple_oauth\Entity\Form\OAuthScopeDeleteForm;
use Drupal\simple_oauth\Entity\Form\OAuthScopeForm;
use Drupal\simple_oauth\LockableConfigEntityAccessControlHandler;
use Drupal\simple_oauth\OAuthScopeListBuilder;
use Drupal\simple_oauth\Services\OAuthServiceInterface;

/**
 * Implements hook_help().
 */
function simple_oauth_help($route_name, RouteMatchInterface $route_match)
{
  switch ($route_name) {
      // Main module help for the simple_oauth module.
    case 'help.page.simple_oauth':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The OAuth 2.0 Authorization Framework: Bearer Token Usage') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_cron().
 */
function simple_oauth_cron()
{
  /** @var OAuthServiceInterface $service */
  $service = Drupal::service('simple_oauth.oauth_service');
  $service->removeExpiredTokens();
  $service->removeExpiredAuthorizationCodes();
}

function simple_oauth_update_8007()
{
  $database = Drupal::database();

  $transaction = $database->startTransaction();

  $tokens = $database->select('access_token')
    ->fields('access_token', ['id', 'value'])
    ->execute()
    ->fetchAllKeyed();

  $manager = Drupal::entityDefinitionUpdateManager();

  $definition = $manager->getFieldStorageDefinition('value', 'access_token');
  $manager->uninstallFieldStorageDefinition($definition);

  // Create a new field definition.
  $definition = BaseFieldDefinition::create('string_long')
    ->setLabel(t('Token'))
    ->setDescription(t('The token value.'))
    ->setSettings(['text_processing' => 0])
    ->setRequired(TRUE)
    ->setDefaultValue('')
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'type' => 'string',
      'weight' => -4,
    ])
    ->setDisplayOptions('form', [
      'type' => 'hidden',
    ])
    ->setDisplayConfigurable('form', FALSE)
    ->setDisplayConfigurable('view', TRUE);
  $manager->installFieldStorageDefinition('value', 'access_token', 'access_token', $definition);

  foreach ($tokens as $id => $token) {
    $database->update('access_token')
      ->fields(['value' => $token])
      ->condition('id', $id)
      ->execute();
  }

  // Commits the transaction.
  unset($transaction);
}

function simple_oauth_update_8006()
{
  //check if the table exists first.  If not, then create the entity.
  if (!Drupal::database()->schema()->tableExists('oauth_app_authorization')) {
    Drupal::entityTypeManager()->clearCachedDefinitions();
    Drupal::entityDefinitionUpdateManager()
      ->installEntityType(Drupal::entityTypeManager()->getDefinition('oauth_app_authorization'));

    return 'OAuth App Authorization installed';
  }

  return 'OAuth App Authorization already exists';
}

function simple_oauth_update_8005()
{
  //check if the table exists first.  If not, then create the entity.
  if (!Drupal::database()->schema()->tableExists('oauth_authorization_code')) {
    Drupal::entityTypeManager()->clearCachedDefinitions();
    Drupal::entityDefinitionUpdateManager()
      ->installEntityType(Drupal::entityTypeManager()->getDefinition('oauth_authorization_code'));

    return 'OAuth Authorization Code installed';
  }

  return 'OAuth Authorization Code already exists';
}

function simple_oauth_update_8004()
{
  $entityTypeManager = Drupal::entityTypeManager();
  $entityType = $entityTypeManager->getDefinition('access_token');

  /** @var FieldStorageDefinitionInterface[] $fieldDefinitions */
  $fieldDefinitions = AccessToken::baseFieldDefinitions($entityType);

  $updateManager = Drupal::entityDefinitionUpdateManager();
  $updateManager->installFieldStorageDefinition('ip', 'access_token', 'simple_oauth', $fieldDefinitions['ip']);
  $updateManager->installFieldStorageDefinition('user_agent', 'access_token', 'simple_oauth', $fieldDefinitions['user_agent']);

  return 'IP & User-Agent fields added to the Access Token Entity';
}

function simple_oauth_update_8003()
{
  $entityTypeManager = Drupal::entityTypeManager();
  $entityType = $entityTypeManager->getDefinition('access_token');

  /** @var FieldStorageDefinitionInterface[] $fieldDefinitions */
  $fieldDefinitions = AccessToken::baseFieldDefinitions($entityType);

  $updateManager = Drupal::entityDefinitionUpdateManager();
  $updateManager->installFieldStorageDefinition('scopes', 'access_token', 'simple_oauth', $fieldDefinitions['scopes']);
  $updateManager->installFieldStorageDefinition('oauth_app', 'access_token', 'simple_oauth', $fieldDefinitions['oauth_app']);

  return 'App and Scopes fields added to the Access Token entity';
}

function simple_oauth_update_8002()
{
  //check if the table exists first.  If not, then create the entity.
  if (!Drupal::database()->schema()->tableExists('oauth_app')) {
    Drupal::entityTypeManager()->clearCachedDefinitions();
    Drupal::entityDefinitionUpdateManager()
      ->installEntityType(Drupal::entityTypeManager()->getDefinition('oauth_app'));

    return 'OAuth App installed';
  }

  return 'OAuth App already exists';
}

function simple_oauth_update_8001()
{
  Drupal::entityDefinitionUpdateManager()->installEntityType(new ConfigEntityType([
    'id' => 'oauth_scope',
    'label' => new TranslatableMarkup('OAuth Scope'),
    'handlers' => [
      "list_builder" => OAuthScopeListBuilder::class,
      "form" => [
        "add" => OAuthScopeForm::class,
        "edit" => OAuthScopeForm::class,
        "delete" => OAuthScopeDeleteForm::class
      ],
      "access" => LockableConfigEntityAccessControlHandler::class
    ],
    'config_prefix' => 'oauth_scope',
    'admin_permission' => 'administer site configuration',
    'entity_keys' => [
      'id' => 'id',
      "label" => "label",
      "uuid" => "uuid"
    ],
    "links" => [
      "canonical" => "/admin/config/people/accounts/oauth_scope/{oauth_scope}",
      "edit-form" => "/admin/config/people/accounts/oauth_scope/{oauth_scope}/edit",
      "delete-form" => "/admin/config/people/accounts/oauth_scope/{oauth_scope}/delete",
      "collection" => "/admin/config/people/accounts/visibility_group"
    ],
    'config_export' => [
      'id',
      'label',
      'locked',
      'description',
    ]
  ]));

  return 'Created OAuth Scope config entity';
}

/**
 * Implements hook_theme().
 */
function simple_oauth_theme($existing, $type, $theme, $path)
{
  return [
    'access_token' => [
      'render element' => 'access_token',
      'file' => 'access_token.page.inc',
    ],
    'oauth_app' => [
      'render element' => 'oauth_app',
      'file' => 'oauth_app.page.inc',
    ],
    'oauth_app_authorization' => [
      'render element' => 'oauth_app_authorization',
      'file' => 'oauth_app_authorization.page.inc',
    ],
    'oauth_authorization_code' => [
      'render element' => 'oauth_authorization_code',
      'file' => 'oauth_authorization_code.page.inc',
    ],
  ];
}
